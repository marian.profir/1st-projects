var dob = 1990;
var now = 2019;
var howManyDays = function (dob, now){

	var substract = now - dob;
	var days = substract * 365;
        return  (`you have lived for ${days} days already!`);
}
module.exports = {
    now, dob, howManyDays
}