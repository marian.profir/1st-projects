var age=29;
var end_age = 70;
var teas_day= 2;
var howManyTeas = function(age, end_age, teas_day){
	
	var left = end_age - age;
	var average = left *365* teas_day;

    return average;
}

module.exports = {
    howManyTeas,
    age,
    end_age,
    teas_day
}