var budgetTracker = function(arr){

        var total = 0;
        for (var i = 0; i < arr.length; i++) {
            total += parseInt(arr[i]);
        }
        
        var divide = total / 7;
        var result = divide * 0.0089;

        return Math.round(result);

}

module.exports = {
    budgetTracker
}