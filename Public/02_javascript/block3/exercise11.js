var lowerCaseLetters = function(str){
	var arr = []; 
	
	for (var i = 0; i < str.length; i++) {
		if (str[i] >= 'A' && str[i] <= 'z') arr += str[i];
		arr = arr.replace(/([a-z])([A-Z])/g, '$1 $2');
	}
	 
	
	 return arr.toLowerCase();

}

module.exports = {
	lowerCaseLetters
}