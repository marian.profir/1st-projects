var twoArrays = function (arr, arr2) {

    var count = 0;
    var max = arr.length > arr2.length ? arr2.length : arr.length;
    for (var i = 0; i < max; i++) {
        if (arr[i] == arr2[i]) {
            count++;
        }
    }
    return count;
}


module.exports = {
    twoArrays
}