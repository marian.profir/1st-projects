var shortener = function(str){
    
    var splitStr = str.toLowerCase().split(' ');
      for (var i = 0; i < splitStr.length; i++) {
          
          splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
          
            str = splitStr.join(' ');
      }
          

    var split_names = splitStr;
    if (split_names.length > 1) {
        return (split_names[0] + " " + split_names[1].charAt(0) + ".");
        
    }

    return  split_names[0];
}
module.exports = {
    shortener
}