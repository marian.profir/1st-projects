var reverser = function(str) {
    var reversed =[];
    for (var i = str.length - 1; i >= 0; i--) {
        reversed.push(str[i]);
  }

  return reversed.join('');
}
module.exports = {
    reverser
}
